# 2020-DEVOXX-FR

🚧 this is a work in progress

Dans son format atelier, les sujets traités seront les suivants:
- Préparation de Kube (K3S/K3D)
- kubectl
- kustomize
- local storage
- traefik v2 (parce que c’est plus drôle que de se contenter de la version proposé par k3s)
- metric server (optionnel)
- intégration avec gitlab
- gitlab core -  (community edition)
- le(s) runner (dans kube)
- Et peut-être plus (registry en mirror, …), mais là il y a déjà de quoi faire

## Prérequis pour le workshop

Selon Louis (pro Linux) les prérequis sont d’avoir un environnement linux ou mac avec docker et git (ce peut être une VM) avec au moins 4 CPU et 6 GO RAM disponibles (gitlab c’est gourmand 😅).

Selon Philippe (pro Mac), essayez d’avoir quelque chose d’un peu plus costaud au niveau de la RAM (8 c’est bien, 16 c’est mieux)

Tout ça pour dire, que si vous êtes sous Linux ça devrait mieux se passer 😉

Remarques: 
- Si vous venez avec du Windows et que cela ne fonctionne pas, on ne pourra rien pour vous. 
- Si vous venez avec un Linux 32, on ne pourra rien pour vous non plus
- Si vous n’avez pas installé les pré-requis (et testé) on ne pourra pas vous attendre (pensez à ceux qui ont fait tout ce qu’il fallait avant de venir)

Si vous n’avez pas d’ordinateur, cela reste intéressant et on vous donnera tous les éléments pour le reproduire chez vous
