# Déployez un environnement GitOps dans du Kube … En local! (Ou le Dév qui voulait être aussi gros que l’Ops)

Discussion il y a quelques mois entre Louis et Philippe:

Phil: Kube c’est compliqué à installer 🥵 et ne me parle pas de MiniKube, quand je le lance mon Mac décolle. J’aimerais apprendre, mais je crois que je vais laisser tomber

Louis: en même temps, c’est un Mac 😂… Mais essaye K3S (un Kube allégé), ou mieux K3D (du K3S dans Docker), simple et facile. Si tu veux je te fais même les scripts d’installation

Phil: Ah oui!? 🤔(ton intéressé)… Et tu saurais déployer un GitLab sur K3D … au hasard?

Louis: ça doit pas être bien compliqué (suffit de lire la doc 😔), je te fais ça à 3 conditions: tu testes mes scripts, m’expliques GitLab CI et fais des scripts d’exemples

Phil: Deal!… Mais alors, on aurait tout ce qu’il faut pour que tous les devs puissent monter facilement une “forge” sur Kube en local sur leur machine?

Louis: c’est ça, et si on documente, on pourrait en faire un workshop 😉

Phil: chiche!

Et depuis ce jour, Louis peaufine des scripts de setup pour K3D+GitLab, Philippe les teste. Et aujourd'hui la V1 fonctionne 🎉

PS : Ok, Kube et Gitlab sur votre machine c’est overkill, mais être raisonnable c’est ennuyeux 

Prérequis: https://gitlab.com/k3g/workshops/2020-devoxx-fr
